import 'package:flutter/material.dart';
import 'package:flutter_wallet_ui_challenge/src/data/data.dart';
import 'package:flutter_wallet_ui_challenge/src/pages/overview_page.dart';
import 'package:flutter_wallet_ui_challenge/src/utils/screen_size.dart';
import 'package:flutter_wallet_ui_challenge/src/widgets/credit_card.dart';
import 'package:flutter_wallet_ui_challenge/src/widgets/payment_card.dart';
import 'package:background_fetch/background_fetch.dart';
import 'dart:async';
import 'package:latlong/latlong.dart';

import 'dart:io';
import 'dart:math' as math;

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

import 'package:location/location.dart';
import 'dart:core';
import 'dart:math' show cos, sqrt, asin;
import 'package:flutter/services.dart' show rootBundle;

import 'package:geolocator/geolocator.dart';
/// Assumes the given path is a text-file-asset.
///
/// Future<String> getFileData(String path) async {
//  return await rootBundle.loadString(path);

bool isNumeric(String str) {
  try{
    double value = double.parse(str);
    if (value > 0){
      return true;
    }
  } on FormatException {
    return false;
  }
}


Future<bool> venuebar(String venue_filepath, String bar_filepath, double currentgeox, double currentgeoy) async {

  String readtextfile = await rootBundle.loadString(venue_filepath);
  List<String> datatextsplit = readtextfile.split('\n');

  bool close = false;


  for (int i = 1; i < datatextsplit.length-1; i++) {
    List<String> column = datatextsplit[i].split(',');

    if (isNumeric(column[9]) == true && isNumeric(column[10]) == true) {
//      double distanceInMeters = await Geolocator().distanceBetween(52.2165157, 6.9437819, 52.3546274, 4.8285838);
      double venuegeox = double.parse(column[9]);
      double venuegeoy = double.parse(column[10]);
      double distanceInMeters = await Geolocator().distanceBetween(venuegeox, venuegeoy, currentgeox, currentgeoy);

      if (distanceInMeters <= 20){
        close = true;

      };

    };

  }
  return(close);

}

Future<bool> bar_visit(String bars_filepath, String venue_filepath, double currentgeox, double currentgeoy) async {
  String readfile = await rootBundle.loadString(bars_filepath);
  List<String> datasplit = readfile.split('\n');
  double bargeox = 0;
  double bargeoy = 0;
  bool close = false;


  for (int i = 1; i < datasplit.length-1; i++) {
    List<String> column = datasplit[i].split(',');
    if (isNumeric(column[9]) == true && isNumeric(column[10]) == true){
      print([[column[9]], column[10]]);


    };

  }
  return(close);

}




class HomePage extends StatelessWidget {

  Future<void> initPlatformState() async {
    // Configure BackgroundFetch.
    // Step 1:  Configure BackgroundFetch as usual.
    BackgroundFetch.configure(BackgroundFetchConfig(
        minimumFetchInterval: 15
    ), (String taskId) async {
      // This is the fetch-event callback.
      print("[BackgroundFetch] taskId: $taskId");

      // Use a switch statement to route task-handling.
      switch (taskId) {
        case 'com.transistorsoft.customtask':
          print("Received custom task");

          Location location = new Location();
          LocationData _locationData;

          _locationData = await location.getLocation();

          final double latitude = _locationData.latitude;
          final double longititude = _locationData.longitude;

          //compute_visit('assets/res/Bars_and_pubs__with_patron_capacity.csv', latitude, longititude);
          final bool music_venue_visist = await venuebar('assets/res/Bars_and_pubs__with_patron_capacity.csv', 'assets/res/Live_Music_Venues.csv', latitude, longititude);
          final bool  bar_visist = await bar_visit('assets/res/Bars_and_pubs__with_patron_capacity.csv', 'assets/res/Live_Music_Venues.csv', latitude, longititude);


         // print(_locationData.lat);


          break;
        default:
          print("Default fetch task");
      }
      // Finish, providing received taskId.
      BackgroundFetch.finish(taskId);
    });

// Step 2:  Schedule a custom "oneshot" task "com.transistorsoft.customtask" to execute 5000ms from now.
    BackgroundFetch.scheduleTask(TaskConfig(
        taskId: "com.transistorsoft.customtask",
        delay: 1000  // <-- milliseconds
    ));
  }

  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context).size;
    //SensorData();
    initPlatformState();
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.zero,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Container(
            color: Colors.grey.shade50,
            height: _media.height / 2,
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Expanded(
                      flex: 5,
                      child: Stack(
                        children: <Widget>[
                          Material(
                            elevation: 4,
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage("assets/images/bg1.jpg"),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Opacity(
                            opacity: 0.3,
                            child: Container(
                              color: Colors.black87,
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(),
                    )
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 20,
                    ),
                    height: _media.longestSide <= 775
                        ? _media.height / 4
                        : _media.height / 4.3,
                    width: _media.width,
                    child:
                        NotificationListener<OverscrollIndicatorNotification>(
                      onNotification: (overscroll) {
                        overscroll.disallowGlow();
                      },
                      child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        padding: EdgeInsets.only(bottom: 10),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: getCreditCards().length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: GestureDetector(
                              onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => OverviewPage())),
                              child: CreditCard(
                                card: getCreditCards()[index],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: _media.longestSide <= 775
                      ? screenAwareSize(20, context)
                      : screenAwareSize(35, context),
                  left: 10,
                  right: 10,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              "",
                              style: TextStyle(
                                  fontSize: _media.longestSide <= 775 ? 35 : 40,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "Varela"),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            color: Colors.grey.shade50,
            width: _media.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    left: 25.0,
                    bottom: 15,
                    top: 15,
                  ),
                  child: Text(
                    "23 july 2019",
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    NotificationListener<OverscrollIndicatorNotification>(
                      onNotification: (overscroll) {
                        overscroll.disallowGlow();
                      },
                      child: ListView.separated(
                        physics: ClampingScrollPhysics(),
                        shrinkWrap: true,
                        separatorBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.only(left: 85.0),
                            child: Divider(),
                          );
                        },
                        padding: EdgeInsets.zero,
                        itemCount: getPaymentsCard().length,
                        itemBuilder: (BuildContext context, int index) {
                          return PaymentCardWidget(
                            payment: getPaymentsCard()[index],
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
